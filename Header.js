// src/components/Header.js
import React from 'react';
import './Header.css';

const Header = () => {
  return (
    <header className="header">
      <h1 className="header-title">REPLAY</h1>
      <div className="header-buttons">
        <button className="header-button selected">
         
          Sleeve
        </button>
        <button className="header-button">
          Umbra
        </button>
      </div>
      <div className="header-links">
        <a href="#" className="header-link">Updates</a>
        <a href="#" className="header-link">Help</a>
      </div>
      <div className="close-button">×</div>
    </header>
  );
};

export default Header;
