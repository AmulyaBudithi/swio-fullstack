import React, { useState } from 'react';
import './Artwork.css';

const Artwork = () => {
  const [size, setSize] = useState('M');
  const [shadow, setShadow] = useState('None');
  const [lighting, setLighting] = useState('None');

  return (
    <div className="artwork">
      <h2>Artwork</h2>
      <div className="controls">
        <div className="size-control">
          <label>Size</label>
          <div className="buttons">
            {['S', 'M', 'L', 'XL'].map(s => (
              <button 
                key={s} 
                className={size === s ? 'active' : ''}
                onClick={() => setSize(s)}
              >{s}</button>
            ))}
          </div>
        </div>
        <div className="shadow-control">
          <label>Shadow</label>
          <div className="buttons">
            {['None', 'Subtle', 'Regular', 'Deep'].map(s => (
              <button 
                key={s} 
                className={shadow === s ? 'active' : ''}
                onClick={() => setShadow(s)}
              >{s}</button>
            ))}
          </div>
        </div>
        <div className="lighting-control">
          <label>Lighting</label>
          <div className="buttons">
            {['None', 'Spotlight', 'Glare'].map(l => (
              <button 
                key={l} 
                className={lighting === l ? 'active' : ''}
                onClick={() => setLighting(l)}
              >{l}</button>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Artwork;

