import React from 'react';
import './Artwork.css';

const Artwork = () => {
  return (
    <div className="artwork">
      <div className="text-content">
        <h1>Artwork</h1>
        <p>
          Scale artwork all the way up or all the way down. Round the corners or leave them square.
          Choose shadow and lighting effects to bring your album artwork to life. Or hide it
completely.
</p>
</div>
<div className="image-content">
{/* Replace these paths with actual paths to your images */}
<img src="/Artwork1.jpg" alt="Artwork Size Options" />
<img src="/Artwork2.jpg" alt="Artwork Shadow Options" />
<img src="/Artwork3.jpg" alt="Artwork Lighting Options" />
</div>
</div>
);
};


export default Artwork;