
import React from 'react';
import './Typography.css';

const Typography = () => {
  return (
    <div className="typography">
      <div className="text-content">
        <h2>Typography</h2>
        <p>
          Pick the track info you want to display, and then exactly how to display it.
          Choose the fonts, weights, sizes, and transparency to use for each line,
          along with customizing color and shadow.
        </p>
      </div>
      <div className="image-content">
        {/* Replace these paths with actual paths to your images */}
        <img src="Typography1.jpg" alt="Color Mode Options" />
        <img src="Typography2.jpg" alt="Font Options" />
      </div>
    </div>
  );
};

export default Typography;
