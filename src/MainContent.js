// src/components/MainContent.js
import React from 'react';
import './MainContent.css';

const MainContent = () => {
  return (
    <div className="main-content">
      <div className="album">
        <img src="/ScreenshotAlbum1.png" alt="Eternal Sunshine" className="album-art" />
        <div className="album-info">
          <p className="album-title">Eternal Sunshine</p>
          <p className="album-artist">Forgotten Feels</p>
          <p className="album-artist">Slow Magic</p>
        </div>

      </div>
      <div className="theme">
        <img src = "/AppSleeve.png" alt="App Sleeve" />
      </div>
      <div className="time-display">Tue 21 May 15:07</div>
    </div>
  );
};

export default MainContent;
