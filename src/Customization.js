// src/components/Customization.js
import React from 'react';
import './Customization.css';

const Customization = () => {
  return (
    <section className="customization">
      <h2>Countless ways to customize.</h2>
      <p>Customization is at the core of the Sleeve experience — choose from any combination of design choices, behaviors, and settings to make Sleeve at home on your desktop.</p>
      <div className="customization-images">
        <img src="/IconArrayTrack.png" alt="Customization" />
        <img src="/IconArrayInterface.png" alt="Customization" />
        <img src="/IconArrayTheme.png" alt="Customization" />
        <img src="/IconArrayLayout.png" alt="Customization" />
        <img src="/IconArrayHotkeys.png" alt="Customization" />
        <img src="/IconArrayPosition.png" alt="Customization" />



      </div>
    </section>
  );
};

export default Customization;
